#!/usr/bin/env bash
# Used by the `debian-packages.sh` script, this helper script will
# runs the cmake configuration in a source folder given by the
# first (and only) command line argument, which must point to the root
# of a BCHN source tree.
#
# e.g.
# $ ./configure_cmake.sh /path/to/bitcoin-cash-node

export LC_ALL=C

set -euxo pipefail

test -d "$1" && test -f "$1/contrib/gitian-signing/keys.txt"

: "${TOPLEVEL:=$1}"
: "${BUILD_DIR:=${TOPLEVEL}/build}"

# Default to nothing
: "${CMAKE_FLAGS:=}"

git clean -xffd
mkdir -p "${BUILD_DIR}"
cd ${BUILD_DIR}

read -a CMAKE_FLAGS <<< "${CMAKE_FLAGS}"
cmake -GNinja .. -DENABLE_CLANG_TIDY=OFF "${CMAKE_FLAGS[@]}" -DCLIENT_VERSION_IS_RELEASE=ON
